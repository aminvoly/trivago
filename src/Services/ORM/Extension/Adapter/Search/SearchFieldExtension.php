<?php

namespace App\Services\ORM\Extension\Adapter\Search;

use App\Services\ORM\Extension\Adapter\AbstractQueryBuilderExtension;
use App\Services\ORM\Extension\QueryAliasNameGenerator;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SearchFieldExtension.
 */
final class SearchFieldExtension extends AbstractQueryBuilderExtension
{
    /**
     * @var array
     */
    private static $operatorsToMethodMap = [
        'BETWEEN' => 'between',
        'IN' => 'in',
        'LIKE' => 'like',
        '>' => 'gt',
        '>=' => 'gte',
        '<' => 'lt',
        '<=' => 'lte',
        '!=' => 'neq',
        '=' => 'eq',
        'NULL' => 'isNull',
    ];

    /**
     * {@inheritdoc}
     */
    public function applyToCollection(QueryBuilder $queryBuilder, string $resourceClass, array &$context)
    {
        if (null === $filters = $context['filter'] ?? null) {
            return;
        }

        if (empty($filters)) {
            return;
        }

        if (!isset($context['aliases']['root'])) {
            $context['aliases']['root'] = $queryBuilder->getRootAliases()[0];
        }

        foreach ($filters as $field => $value) {
            if ($this->isNested($field) || false === $this->hasField($resourceClass, $field)) {
                continue;
            }

            $operator = $this->getOperator($value);
            $alias = sprintf('%s.%s', $context['aliases']['current'] ?? $context['aliases']['root'], $field);
            $method = self::$operatorsToMethodMap[$operator];

            if ('BETWEEN' === $operator) {
                $firstParameter = QueryAliasNameGenerator::generate($alias);
                $secondParameter = $firstParameter.'_2';

                $queryBuilder->andWhere($queryBuilder->expr()->between($alias, ":$firstParameter", ":$secondParameter"))
                    ->setParameter($firstParameter, $value[0])
                    ->setParameter($secondParameter, $value[1]);
            } elseif ('NULL' === $operator) {
                $queryBuilder->andWhere($queryBuilder->expr()->isNull($alias));
            } else {
                $parameterName = QueryAliasNameGenerator::generate($alias);

                $queryBuilder->andWhere($queryBuilder->expr()->{$method}($alias, ":{$parameterName}"))
                    ->setParameter($parameterName, $value);
            }
        }
    }

    /**
     * @param $value
     *
     * @return string
     */
    private function getOperator(&$value): string
    {
        if (!is_array($value)) {
            $value = $this->normalizeValue($value);

            return '=';
        }

        $operator = key($value);
        $value = $this->normalizeValue($value[$operator]);

        switch ($operator) {
            case 'btn':
                return 'BETWEEN';
            case 'in':
                return 'IN';
            case 'like':
                return 'LIKE';
            case 'gt':
                return '>';
            case 'gte':
                return '>=';
            case 'lt':
                return '<';
            case 'lte':
                return '<=';
            case 'neq':
                return '!=';
            case 'null':
                return 'NULL';
            default:
                return '=';
        }
    }

    /**
     * @param $value
     *
     * @return array|int
     */
    private function normalizeValue($value)
    {
        if (false !== strpos($value, ',')) {
            $value = array_map(
                function ($v) {
                    return is_numeric($v) ? ($v + 0) : $v;
                },
                array_values(array_filter(array_map('trim', explode(',', $value))))
            );
        } elseif (is_numeric($value)) {
            $value += 0;
        } elseif ('null' === $value) {
            $value = null;
        }

        return $value;
    }
}
