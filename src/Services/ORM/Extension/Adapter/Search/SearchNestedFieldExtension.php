<?php

namespace App\Services\ORM\Extension\Adapter\Search;

use App\Services\ORM\Extension\Adapter\AbstractQueryBuilderExtension;
use App\Services\ORM\Extension\QueryAliasNameGenerator;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SearchNestedFieldExtension.
 */
final class SearchNestedFieldExtension extends AbstractQueryBuilderExtension
{
    /**
     * @var SearchFieldExtension
     */
    private $searchFieldExtension;

    /**
     * SearchNestedFieldExtension constructor.
     *
     * @param SearchFieldExtension $searchFieldExtension
     * @param ManagerRegistry      $managerRegistry
     */
    public function __construct(SearchFieldExtension $searchFieldExtension, ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry);
        $this->searchFieldExtension = $searchFieldExtension;
    }

    /**
     * {@inheritdoc}
     */
    public function applyToCollection(QueryBuilder $queryBuilder, string $resourceClass, array &$context)
    {
        if (null === $filters = $context['filter'] ?? null) {
            return;
        }

        if (empty($filters)) {
            return;
        }

        if (!isset($context['aliases']['root'])) {
            $context['aliases']['root'] = $queryBuilder->getRootAliases()[0];
        }

        $rootAlias = $context['aliases']['root'];

        foreach ($filters as $field => $value) {
            if (!$this->isNested($field)) {
                continue;
            }

            $resource = $resourceClass;
            $parts = explode('.', $field);
            $field = array_pop($parts);
            $associations = $parts;

            foreach ($associations as $association) {
                $associationTargetClass = $this->getAssociationTargetClass($resource, $association);

                if (isset($context['aliases'][$resource][$associationTargetClass])) {
                    $rootAlias = $context['aliases'][$resource][$associationTargetClass];
                    $context['aliases']['current'] = $rootAlias;
                    continue;
                }

                $relationAliasName = QueryAliasNameGenerator::generate($rootAlias.'.'.$association);

                if (null === $relationAliasName) {
                    $resource = $associationTargetClass;
                    $rootAlias = QueryAliasNameGenerator::getJoinAlias($queryBuilder, sprintf('%s.%s', $rootAlias, $association));
                    $context['aliases']['current'] = $rootAlias;

                    continue;
                }

                $context['aliases']['current'] = $relationAliasName;
                $context['aliases'][$resource][$associationTargetClass] = $relationAliasName;
                $resource = $associationTargetClass;

                $queryBuilder->join($rootAlias.'.'.$association, $relationAliasName);

                $rootAlias = $relationAliasName;
            }

            $newContext = array_merge($context, ['filter' => [$field => $value]]);

            $this->searchFieldExtension->applyToCollection($queryBuilder, $resource, $newContext);

            unset($context['aliases']['current']);
        }
    }
}
