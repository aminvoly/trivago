<?php

namespace App\Services\ORM\Extension\Adapter\Sort;

use App\Services\ORM\Extension\Adapter\AbstractQueryBuilderExtension;
use App\Services\ORM\Extension\SortParameterNormalizer;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SortExtension.
 */
final class SortFieldExtension extends AbstractQueryBuilderExtension
{
    /**
     * {@inheritdoc}
     */
    public function applyToCollection(QueryBuilder $queryBuilder, string $resourceClass, array &$context)
    {
        if (null === $sorts = $context['sort'] ?? null) {
            return;
        }

        if (!$sorts) {
            return;
        }

        if (!isset($context['aliases']['root'])) {
            $context['aliases']['root'] = $queryBuilder->getRootAliases()[0];
        }

        foreach (new SortParameterNormalizer($sorts) as $sortData) {
            $field = $sortData['field'];

            if ($this->isNested($field) || !$this->hasField($resourceClass, $field)) {
                continue;
            }

            $alias = sprintf('%s.%s', $context['aliases']['current'] ?? $context['aliases']['root'], $field);

            $queryBuilder->orderBy($alias, $sortData['direction']);
        }
    }
}
