<?php

namespace App\Services\ORM\Extension\Adapter\Sort;

use App\Services\ORM\Extension\Adapter\AbstractQueryBuilderExtension;
use App\Services\ORM\Extension\QueryAliasNameGenerator;
use App\Services\ORM\Extension\SortParameterNormalizer;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SortNestedFieldExtension.
 */
final class SortNestedFieldExtension extends AbstractQueryBuilderExtension
{
    /**
     * @var SortFieldExtension
     */
    private $sortFieldExtension;

    /**
     * SortNestedFieldExtension constructor.
     *
     * @param SortFieldExtension $sortFieldExtension
     * @param ManagerRegistry    $managerRegistry
     */
    public function __construct(SortFieldExtension $sortFieldExtension, ManagerRegistry $managerRegistry)
    {
        $this->sortFieldExtension = $sortFieldExtension;
        parent::__construct($managerRegistry);
    }

    /**
     * {@inheritdoc}
     */
    public function applyToCollection(QueryBuilder $queryBuilder, string $resourceClass, array &$context)
    {
        if (null === $sorts = $context['sort'] ?? null) {
            return;
        }

        if (!$sorts) {
            return;
        }

        $context['aliases']['root'] = $context['aliases']['root'] ?? $queryBuilder->getRootAliases()[0];
        $rootAlias = $context['aliases']['root'];
        $resource = $resourceClass;

        foreach (new SortParameterNormalizer($sorts) as $sortData) {
            $field = $sortData['field'];

            if (!$this->isNested($field)) {
                continue;
            }

            $parts = explode('.', $field);
            $field = array_pop($parts);
            $associations = $parts;

            foreach ($associations as $association) {
                $associationTargetClass = $this->getAssociationTargetClass($resource, $association);

                if (isset($context['aliases'][$resource][$associationTargetClass])) {
                    $rootAlias = $context['aliases'][$resource][$associationTargetClass];
                    $context['aliases']['current'] = $rootAlias;
                    continue;
                }

                $relationAliasName = QueryAliasNameGenerator::generate($rootAlias.'.'.$association);

                if (null === $relationAliasName) {
                    $resource = $associationTargetClass;
                    $rootAlias = QueryAliasNameGenerator::getJoinAlias(
                        $queryBuilder,
                        sprintf('%s.%s', $rootAlias, $association)
                    );
                    $context['aliases']['current'] = $rootAlias;

                    continue;
                }

                $context['aliases']['current'] = $relationAliasName;
                $context['aliases'][$resource][$associationTargetClass] = $relationAliasName;
                $resource = $associationTargetClass;
                $queryBuilder->leftJoin($rootAlias.'.'.$association, $relationAliasName);

                $rootAlias = $relationAliasName;
            }

            $newContext = array_merge(
                $context,
                [
                    'sort' => [sprintf('%s%s', 'DESC' === $sortData['direction'] ? '-' : '', $field)],
                ]
            );

            $this->sortFieldExtension->applyToCollection($queryBuilder, $resource, $newContext);

            unset($context['aliases']['current']);
        }
    }
}
