<?php


namespace App\Services\Booking;


use App\Entity\Booking;
use App\Entity\Hotel;
use App\Entity\User;
use App\Services\Hotel\HotelService;
use Doctrine\ORM\EntityManagerInterface;

class BookingService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var HotelService
     */
    private $hotelService;

    /**
     * BookingService constructor.
     * @param EntityManagerInterface $manager
     * @param HotelService $hotelService
     */
    public function __construct(EntityManagerInterface $manager, HotelService $hotelService)
    {
        $this->manager = $manager;
        $this->hotelService = $hotelService;
    }

    public function book(User $user, Hotel $hotel)
    {
        if ($this->checkIfAlreadyBookedByUser($user->getId(), $hotel->getId()) || $this->checkAvailability($hotel)) {
            return false;
        }
        $booking = new Booking();
        $booking->setUser($user);
        $booking->setHotel($hotel);
        $this->manager->persist($booking);
        $this->hotelService->updateAvailability($hotel);;
        $this->manager->flush();

        return true;
    }


    private function checkIfAlreadyBookedByUser(int $userId, int $hotelId)
    {
        $result = $this->manager->getRepository(Booking::class)->findBookingByUserAndHotelId($userId, $hotelId);

        return count($result);
    }

    private function checkAvailability(Hotel $hotel)
    {
        return !$hotel->getAvailability();
    }
}
