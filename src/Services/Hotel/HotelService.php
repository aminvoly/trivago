<?php


namespace App\Services\Hotel;


use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;

class HotelService
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * BookingService constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function updateAvailability(Hotel $hotel)
    {
        $hotel->setAvailability($hotel->getAvailability() - 1);
        $this->manager->persist($hotel);
    }
}
