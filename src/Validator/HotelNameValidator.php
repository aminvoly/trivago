<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class HotelNameValidator extends ConstraintValidator
{
    CONST FORBIDDEN_NAMES = ['free', 'offer', 'book', 'website'];

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint HotelName */

        if (!(str_replace(self::FORBIDDEN_NAMES, '', strtolower($value)) !== strtolower($value))) {
            return;
        }

        // TODO: implement the validation here
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }
}
