<?php

namespace App\DataFixtures;

use App\Entity\Location;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class OrderItemFixture.
 */
class LocationFixtures extends BaseFixture
{
    /**
     * {@inheritdoc}
     */
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(
            Location::class,
            10,
            function (Location $item, $i) {

                $item->setCountry($this->faker->country)
                    ->setCity($this->faker->city)
                    ->setAddress($this->faker->address)
                    ->setState($this->faker->state)
                    ->setZipCode($this->faker->numberBetween(10000, 99999));
                $this->setReference('location#'.($i + 1), $item);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 10;
    }

    /**
     * {@inheritdoc}
     */
    protected function getReferenceName(string $className, int $i): string
    {
        return parent::getReferenceName($className, $i).'_'.uniqid();
    }
}
