<?php

namespace App\Controller;

use App\Entity\Hotel;
use App\Entity\User;
use App\Services\Booking\BookingService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class BookingController extends BaseController
{
    /**
     *
     * @Security(name="Bearer")
     *
     * @SWG\Tag(name="Booking")
     * @SWG\Response(
     *     response=201,
     *     description="Book a room, note: you should login with user, and set the authorization key(token) which is Bearer token, then book a room"
     * )
     *
     * @Route("/api/book/{id}", name="booking",methods={"GET"})
     * @param Hotel $hotel
     * @param BookingService $bookingService
     * @return JsonResponse
     */
    public function book(Hotel $hotel, BookingService $bookingService)
    {
        /** @var User $user */
        $user = $this->getUser();
        $result = $bookingService->book($user, $hotel);
        if ($result) {
            return $this->respond(['success' => true, 'message' => 'Room booked successfully'], Response::HTTP_CREATED);
        }

        return $this->respond(
            ['success' => false, 'message' => 'The room is not available or already reserved by you.'],
            Response::HTTP_BAD_REQUEST
        );
    }
}
