<?php

namespace App\Tests;

use App\Entity\Hotel;
use App\Entity\User;

class HotelControllerTest extends BaseControllerTest
{
    public function testIndex()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        $client = $this->getToken($user->getEmail());
        $client->request('GET', '/api/hotels');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        $client = $this->getToken($user->getEmail());
        $client->request(
            'POST',
            '/api/hotels',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(
                [
                    "name"         => $this->faker->text(20),
                    "rating"       => $this->faker->numberBetween(0, 5),
                    "category"     => self::CATEGORIES[array_rand(self::CATEGORIES)],
                    "location"     => [
                        "city"    => $this->faker->city,
                        "state"   => $this->faker->state,
                        "country" => $this->faker->country,
                        "zipCode" => $this->faker->numberBetween(10000, 99999),
                        "address" => $this->faker->address,
                    ],
                    "image"        => $this->faker->imageUrl(),
                    "reputation"   => $this->faker->numberBetween(0, 1000),
                    "price"        => $this->faker->numberBetween(50, 1000),
                    "availability" => $this->faker->numberBetween(1, 100),
                ]
            )
        );
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testCreateFailure()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        $client = $this->getToken($user->getEmail());
        $client->request(
            'POST',
            '/api/hotels',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(
                [
                    "rating"       => $this->faker->numberBetween(0, 5),
                    "category"     => self::CATEGORIES[array_rand(self::CATEGORIES)],
                    "image"        => $this->faker->imageUrl(),
                    "reputation"   => $this->faker->numberBetween(0, 1000),
                    "price"        => $this->faker->numberBetween(50, 1000),
                    "availability" => $this->faker->numberBetween(1, 100),
                ]
            )
        );
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
    }

    public function testUpdate()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        /** @var Hotel $hotel */
        $hotel = $this->createTestHotel($user);
        $client = $this->getToken($user->getEmail());
        $client->request(
            'PUT',
            '/api/hotels/'.$hotel->getId(),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(
                [
                    "name"         => $this->faker->name,
                    "rating"       => $this->faker->numberBetween(0, 5),
                    "category"     => self::CATEGORIES[array_rand(self::CATEGORIES)],
                    "location"     => [
                        "city"    => $this->faker->city,
                        "state"   => $this->faker->state,
                        "country" => $this->faker->country,
                        "zipCode" => $this->faker->numberBetween(10000, 99999),
                        "address" => $this->faker->address,
                    ],
                    "image"        => $this->faker->imageUrl(),
                    "reputation"   => $this->faker->numberBetween(0, 1000),
                    "price"        => $this->faker->numberBetween(50, 1000),
                    "availability" => $this->faker->numberBetween(1, 100),
                ]
            )
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    public function testUpdateFailure()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        /** @var Hotel $hotel */
        $hotel = $this->createTestHotel($user);
        $client = $this->getToken($user->getEmail());
        $client->request(
            'PUT',
            '/api/hotels/'.$hotel->getId(),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(
                [
                    "name"         => $this->faker->name,
                    "rating"       => $this->faker->numberBetween(0, 5),
                    "category"     => self::CATEGORIES[array_rand(self::CATEGORIES)],
                    "location"     => [
                        "city"    => $this->faker->city,
                        "state"   => $this->faker->state,
                        "country" => $this->faker->country,
                        "zipCode" => $this->faker->numberBetween(10000, 99999),
                        "address" => $this->faker->address,
                    ],
                    "image"        => $this->faker->imageUrl(),
                    "reputation"   => $this->faker->numberBetween(0, 1000),
                    "price"        => $this->faker->numberBetween(50, 1000),
                    "availability" => $this->faker->numberBetween(1, 100),
                ]
            )
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testDelete()
    {
        /** @var User $user */
        $user = $this->createTestUser();
        /** @var Hotel $hotel */
        $hotel = $this->createTestHotel($user);
        $client = $this->getToken($user->getEmail());
        $client->request(
            'DELETE',
            '/api/hotels/'.$hotel->getId(),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}
