<?php

namespace App\Tests;

use App\Entity\User;

class BookingControllerTest extends BaseControllerTest
{

    public function testBook()
    {
        /** @var User $user */
        $user = $this->createTestUser(['ROLE_USER']);
        $hotel = $this->createTestHotel($user);
        $client = $this->getToken($user->getEmail());
        $client->request('GET', '/api/book/'.$hotel->getId());
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

}
